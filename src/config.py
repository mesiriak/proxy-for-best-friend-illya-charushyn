from functools import lru_cache
from typing import ClassVar

from pydantic import MongoDsn
from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    # Database envs.
    MONGO_DSN: MongoDsn | None = None  # No need in DB now.

    # API envs.
    API_DEBUG: bool = True
    API_PORT: int = 8000
    API_HOST: str = "0.0.0.0"

    # CRM envs.
    CRM_URL: str = ""
    CRM_API_KEY: str = ""

    # Logger envs.
    LOG_LEVEL: str = "DEBUG"
    LOG_PROXY_REQUESTS: bool = True

    # Proxy client request/connection timeouts.
    CONNECTION_TIMEOUT: int = 600
    REQUEST_TIMEOUT: int = 60

    model_config: ClassVar[SettingsConfigDict] = SettingsConfigDict(
        env_file=".env"
    )


@lru_cache()
def get_settings() -> Settings:
    return Settings()
