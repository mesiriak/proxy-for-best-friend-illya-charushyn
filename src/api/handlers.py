from fastapi import APIRouter, Depends
from fastapi.requests import Request
from fastapi.responses import Response

from src.adapters.proxy.crm import CRMProxyClient
from src.api.dependencies import get_crm_proxy_client

crm_proxy_router = APIRouter(
    prefix="/crm/proxy"
)

@crm_proxy_router.api_route(
    path="/{path:path}",
    methods=["GET", "POST", "PUT",  "DELETE", "OPTIONS"]
)
async def crm_proxy_route(
    path: str,
    request: Request,
    proxy_client: CRMProxyClient = Depends(get_crm_proxy_client),
):
    proxy_path, headers = request.url.path, dict(request.headers)

    proxy_response = await proxy_client.request(
        method=request.method,
        url=proxy_path,
        content=request.stream(),
        headers=headers,
    )
    return Response(
        headers=proxy_response.headers,
        status_code=proxy_response.status_code,
        content=proxy_response.content,
    )
