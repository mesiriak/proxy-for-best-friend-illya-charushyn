from typing import Annotated

from fastapi import Depends

from src.adapters.proxy.crm import CRMProxyClient
from src.config import Settings, get_settings


def get_crm_proxy_client(settings: Annotated[Settings, Depends(get_settings)]) -> CRMProxyClient:
    return CRMProxyClient(
        connection_timeout=settings.CONNECTION_TIMEOUT,
        request_timeout=settings.REQUEST_TIMEOUT,
        log_proxy_factor=settings.LOG_PROXY_REQUESTS,
        crm_url=settings.CRM_URL,
        crm_api_key=settings.CRM_API_KEY,
    )

