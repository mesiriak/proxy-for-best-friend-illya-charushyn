from fastapi import FastAPI

from src.api.handlers import crm_proxy_router
from src.config import Settings
from src.logger import configure_logger


def create_application(settings: Settings) -> FastAPI:
    application = FastAPI()

    application.include_router(crm_proxy_router)

    configure_logger(log_level=settings.LOG_LEVEL)

    return application
