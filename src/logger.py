import logging
import logging.config
from typing import Any


def configure_logger(log_level: str) -> dict[str, Any]:
    config = {
        "version": 1,
        "disable_existing_loggers": False,
        "formatters": {
            "dummy": {
                "format": "%(levelname)-8s | %(asctime)s | %(name)s: %(message)s",
            }
        },
        "handlers": {
            "stdout": {
                "class": "logging.StreamHandler",
                "level": log_level,
                "formatter": "dummy",
                "stream": "ext://sys.stdout",
            },
            "stderr": {
                "class": "logging.StreamHandler",
                "level": "ERROR",
                "formatter": "dummy",
                "stream": "ext://sys.stderr",
            },
        },
        "loggers": {
            "root": {"level": log_level, "handlers": ["stdout"]},
            "uvicorn.error": {
                "level": log_level,
                "handlers": ["stderr"],
                "propagate": True,
            },
        },
    }

    logging.config.dictConfig(config=config)
    return config
