from typing import Any, IO, Iterable, AsyncIterable

DataType = dict[str, Any]
JsonType = dict[str, Any]

PrimitiveDataType = str | int | float | bool

ParamsType = (
    dict[str, PrimitiveDataType]
    | list[tuple[str, PrimitiveDataType]]
    | tuple[tuple[str, PrimitiveDataType], Any]
    | str
    | bytes
)


FileContent = IO[bytes] | str | bytes
FileType = (
    FileContent
    | tuple[str, FileContent]
    | tuple[str, FileContent, str]
    | tuple[str, FileContent, str, dict[str, str]]
)

HeaderType = dict[str, str] | dict[bytes, bytes] | tuple[str, str] | tuple[bytes, bytes]
CookieType = dict[str, str] | list[tuple[str, str]]

ContentType = str | bytes | Iterable[bytes] | AsyncIterable[bytes]
