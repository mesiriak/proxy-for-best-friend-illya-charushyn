import re
from json import loads as json_loads, dumps as json_dumps
from json.decoder import JSONDecodeError

import logging
import uuid
from typing import Any
from urllib.parse import urljoin

from httpx import AsyncClient, Response, Request, Timeout
from src.adapters.utils.typing import (
    FileType,
    HeaderType,
    DataType,
    CookieType,
    ParamsType,
    JsonType,
    ContentType,
)

logger = logging.getLogger(__name__)


class BaseProxyClient:
    _client: AsyncClient | None = None

    def __init__(
        self,
        connection_timeout: int,
        request_timeout: int,
        log_proxy_factor: bool,
        service_url: str,
    ) -> None:
        self.timeout = Timeout(request_timeout, connect=connection_timeout)
        self.log_proxy_factor = log_proxy_factor
        self.service_url = service_url
        self.event_hooks = {
            "request": [_log_request],
            "response": [_log_response],
        }

    @property
    def client(self):
        if self._client is None or self._client.is_closed:
            event_hooks = self.event_hooks if self.log_proxy_factor else {}

            self._client = AsyncClient(timeout=self.timeout, event_hooks=event_hooks)

        return self._client

    async def request(
        self,
        method: str,
        url: str,
        headers: HeaderType | None = None,
        data: DataType | None = None,
        json: JsonType | None = None,
        files: FileType | None = None,
        params: ParamsType | None = None,
        cookies: CookieType | None = None,
        content: ContentType | None = None,
    ) -> Response:
        if headers is None:
            headers = {}

        headers = self.provide_extra_headers(headers=headers)

        # getting service endpoint link here
        _, relative_proxy_url = re.split(r"/proxy/", url)
        service_endpoint = urljoin(base=self.service_url, url=relative_proxy_url)

        response = await self.client.request(
            method=method,
            url=service_endpoint,
            headers=headers,
            data=data,
            json=json,
            files=files,
            params=params,
            cookies=cookies,
            content=content,
        )

        return response

    def provide_extra_headers(self, headers: HeaderType) -> HeaderType:
        new_headers = {}

        for key, value in headers.items():
            # host header blocking nginx work
            # authorization need to be cleared and provided below
            if key.lower() not in {"authorization", "host"}:
                new_headers[key] = value

        new_headers.update(self.get_authorization_headers())
        return new_headers

    def get_authorization_headers(self) -> dict[str, Any]:
        return {"Authorization": ""}

    async def close(self):
        if self._client is not None:
            await self._client.aclose()


async def _log_request(request: Request):
    request.headers["X-SESSION-ID"] = uuid.uuid4().hex
    await request.aread()
    try:
        body = json_loads(request.content.decode("utf-8"))
    except JSONDecodeError:
        body = request.content.decode("utf-8")
    data = {
        "request_id": request.headers.get("X-SESSION-ID"),
        "request": {
            "url": str(request.url),
            "method": request.method,
            "headers": dict(request.headers.items()),
            "body": body,
        },
    }
    logger.debug("\n%s", json_dumps(data, indent=4))


async def _log_response(response: Response):
    await response.aread()
    try:
        body = response.json()
    except JSONDecodeError:
        body = response.content.decode("utf-8")
    data = {
        "request_id": response.request.headers.get("X-SESSION-ID"),
        "response": {
            "status_code": response.status_code,
            "headers": dict(response.headers.items()),
            "body": body,
            "elapsed": str(round(response.elapsed.total_seconds() * 1000)) + "ms",
        },
    }
    logger.debug("\n%s", json_dumps(data, indent=4))
