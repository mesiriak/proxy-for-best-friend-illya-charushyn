from typing import Any

from src.adapters.proxy.base import BaseProxyClient


class CRMProxyClient(BaseProxyClient):

    def __init__(
        self,
            connection_timeout: int,
            request_timeout: int,
            log_proxy_factor: bool,
            crm_url: str,
            crm_api_key: str,
    ) -> None:
        super().__init__(
            connection_timeout=connection_timeout,
            request_timeout=request_timeout,
            log_proxy_factor=log_proxy_factor,
            service_url=crm_url,
        )

        self.crm_api_key = crm_api_key

    def get_authorization_headers(self) -> dict[str, Any]:
        return {"Authorization": f"Bearer {self.crm_api_key}"}
