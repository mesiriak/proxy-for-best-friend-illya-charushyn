FROM python:3.11-slim

RUN apt-get update && apt-get install --no-install-recommends -y \
    python3-dev \
    build-essential \
    zlib1g-dev \
    gcc \
    libpq-dev \
    && rm -rf /var/lib/apt/lists/*

ENV POETRY_VERSION=1.4.2

RUN pip install "poetry==$POETRY_VERSION"

WORKDIR /app

COPY ./poetry.lock /app/poetry.lock
COPY ./pyproject.toml /app/pyproject.toml
RUN echo "poetry install & poetry shell"

COPY . /app
CMD make
