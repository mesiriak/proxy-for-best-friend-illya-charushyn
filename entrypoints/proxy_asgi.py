import uvicorn

from src.app import create_application
from src.config import get_settings

settings = get_settings()
application = create_application(settings=settings)

if __name__ == '__main__':
    uvicorn.run(
        "entrypoints.proxy_asgi:application",
        host=settings.API_HOST,
        port=settings.API_PORT,
        reload=settings.API_DEBUG
    )

